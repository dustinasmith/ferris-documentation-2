Search
======

Ferris provides integration into App Engine's `search api <https://developers.google.com/appengine/docs/python/search/>`_ by providing utilities for indexing :doc:`models <models>` and for retrieving model instances from results.


Indexing Models
---------------

.. module:: ferris.behaviors.searchable

The :class:`Searchable` behavior will automatically add entities to the search index when saved. This is a wrapper around :func:`~ferris.core.search.index_entity`::

    from ferris import Model
    from ferris.behaviors import searchable

    class Post(Model):
        class Meta:
            behaviors = (searchable.Searchable,)

        title = ndb.StringProperty()
        context = ndb.TextProperty()

.. autoclass:: Searchable

This behavior can be configured using the meta class::

    class Meta:
        behaviors = (searchable.Searchable,)
        search_index = 'auto_ix_Post'
        search_exclude = ('thumbnail', 'likes')

.. attribute:: SearchableMeta.search_index

    Which search index to add the entity's data to. By default this is ``auto_ix_[Model]``. You can set it to a list or tuple to add the entity data to muliple indexes

.. attribute:: SearchableMeta.search_fields

    A list or tuple of field names to use when indexing. If not specified, all fields will be used.

.. attribute:: SearchableMeta.search_exclude

    A list or tuple of field names to exclude when indexing.

.. attribute:: SearchableMeta.search_callback

    A callback passed to :func:`~ferris.core.search.index_entity`. This can be used to index additional fields::

        from google.appengine.ext import ndb, search
        from ferris.behaviors.searchable import Searchable

        class Post(Model):
            class Meta:
                behaviors = (Searchable,)

                @static_method
                def search_callback(instance, fields):
                    category = instance.category.get()
                    fields.append(
                        search.TextField(
                            name="category",
                            value=category.title
                        )
                    )

            title = ndb.StringProperty()
            category = ndb.KeyProperty(Category)


.. note:: The searchable behavior can not automatically index Computed, Key, or Blob properties. Use the search_callback to implement indexing for these fields.


Performing Searches
-------------------

.. module:: ferris.components.search

The most common use case for search is to present search results to the user. The :class:`Search` component provides a wrapper around :func:`~ferris.core.search.search` to make querying and index and transforming the results into ndb entities easy::

    from ferris import Controller
    from ferris.components.search import Search

    class Posts(Controller):
        class Meta:
            components = (Search,)

        def list(self):
            return self.components.search()

This component plays well with :doc:`scaffolding <scaffolding>`, :mod:`pagination <ferris.components.pagination>`, and :doc:`messages <messages>`.

.. autoclass:: Search

.. automethod:: Search.search()

When searching, the component:

    * Determines the index to search.
    * If pagination is used, gets the cursor.
    * Performs the search using :func:`~ferris.core.search.search`.
    * Transforms the results into ndb entities.
    * Sets pagination data
    * Sets ``search_query`` and ``search_results`` in the view context.

You can pass in configuration to component when calling :meth:`~Search.search`::

    @route
    def list(self):
        self.components.search(
            'global',
            query=self.request.params.get('query')
        )

Macros
------

.. _search_macros:

The search macros make it easy to interact with the search component from your templates.

Import the search macros using::
    
    {% import "macros/search.html" as search with context %}

.. method:: SearchMacros.filter(uri, examples=None, _pass_all=False)

    Displays a simple search box. If the search action is different from the current action be sure to specify a URI (i.e. for global search)::

        {{search.filter(uri('posts:search'))}}

.. method:: SearchMacros.info()

    Displays information about the search. If the search returns no results or if the user submits an invalid query will display a friendly message to the user.


If the look and feel of the search macros needs to be customized for your application, feel free to copy them into ``app/templates/macros/search.html`` and modified as needed.


Advanced Usage
--------------

.. module:: ferris.core.search

The :mod:`search` module provides the functionality that's wrapped by the searchable behavior and the search component. You can use this directly to have greater control over how things are indexed or search. You can also use :func:`search` directly to search outside of a controller context.

.. autofunction:: index_entity

.. autofunction:: unindex_entity

.. autofunction:: search

.. autofunction:: join_query

.. autofunction:: transform_to_entities

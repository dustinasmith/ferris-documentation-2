Memcache
========

Ferris provides a couple of decorators to help with using the `App Engine Memcache API <https://developers.google.com/appengine/docs/python/memcache/>`_. Caching items in memcache can significantly reduce the cost of running your application while at the same time making your application more responsive.

.. module:: ferris.core.memcache

.. autofunction:: cached

    For example::

        from ferris import cached

        @cached('my-key')
        def do_something_slow():
            return herd_cats()

        # First call will prime memcache, subsequent calls will get the result from memcache.
        r = do_something_slow()

        # This skips memcache and always returns a fresh value. Does not update memcache.
        r = do_something_slow.uncached()

        # This clears the value in memcache so that the subsequent call returns a fresh value.
        do_something_slow.clear_cache()

.. autofunction:: cached_by_args

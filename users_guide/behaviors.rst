Behaviors
=========

.. module:: ferris.core.ndb.behavior

Behaviors are reusable pieces of functionality that can be used via a model and can react to model events. Behaviors are an analogue of :doc:`components <components>` designed for models instead of controllers.

Using Behaviors
---------------

To use a behavior, include it in your model's ``meta`` class::

    class Post(Model):
        class Meta:
            behaviors = (SomeBehavior,)

Most behavaiors also get their configuration from the meta class::
    
    class Meta:
        behaviors = (SomeBehavior,)
        somebehavior_someconfig = 'test'


Creating Behaviors
------------------

To create a behavior subclass :class:`Behavior`. Here's a example that underscores the specified fields in ``Meta.underscore_fields``::

    from ferris import Behavior

    class Underscorer(Behavior):
        def before_put(self, instance):
            for field in instance.Meta.underscore_fields:
                val = getattr(instance, field)
                underscored = inflector.underscore(val)
                setattr(instance, field, underscored)

.. autoclass:: Behavior


Behaviors can respond to the same :ref:`callbacks <model_callbacks>` as models.

.. automethod:: Behavior.before_put(self, instance)
.. automethod:: Behavior.after_put(self, instance)
.. automethod:: Behavior.before_delete(self, key)
.. automethod:: Behavior.after_delete(self, key)
.. automethod:: Behavior.before_get(self, key)
.. automethod:: Behavior.after_get(self, item)
